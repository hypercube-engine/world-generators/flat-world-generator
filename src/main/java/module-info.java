import net.hypercube.worldgenerators.flatworldgenerator.FlatWorldGenerator;

module net.hypercube.worldgenerators.flatworldgenerator {
	exports net.hypercube.worldgenerators.flatworldgenerator;

	requires transitive net.hypercube.api;

	provides net.hypercube.api.worldgenerators.IWorldGenerator with FlatWorldGenerator;
}