package net.hypercube.worldgenerators.flatworldgenerator;

import java.util.EnumMap;
import java.util.function.Function;
import java.util.stream.IntStream;

import org.joml.Vector3i;

import net.hypercube.api.blocks.AbstractBlock;
import net.hypercube.api.chunks.Chunk;
import net.hypercube.api.chunks.ChunkUtils;
import net.hypercube.api.chunks.SimpleChunk;
import net.hypercube.api.worldgenerators.IRequiredAlias;
import net.hypercube.api.worldgenerators.IWorldGenerator;

public class FlatWorldGenerator implements IWorldGenerator {
	private static final int CHUNK_SIZE = 15;

	private final EnumMap<? extends IRequiredAlias, Function<Vector3i, ? extends AbstractBlock>> blockSuppliers;

	public FlatWorldGenerator() {
		blockSuppliers = new EnumMap<>(RequiredAlias.class);
	}

	@Override
	public EnumMap<? extends IRequiredAlias, Function<Vector3i, ? extends AbstractBlock>> getRequiredBlockClasses() {
		return blockSuppliers;
	}

	@Override
	public int getChunkEdgeLength() {
		return CHUNK_SIZE;
	}

	@Override
	public Chunk generateChunk(Vector3i centerPosition) {
		final Chunk chunk = new SimpleChunk(centerPosition, CHUNK_SIZE);

		int lowEnd = chunk.getLowEnd();
		int highEnd = chunk.getHighEnd();

		IntStream.range(lowEnd, highEnd + 1).forEach(x -> {
			IntStream.range(lowEnd, highEnd + 1).forEach(y -> {
				IntStream.range(lowEnd, highEnd + 1).forEach(z -> {
					Vector3i posInChunk = new Vector3i(x, y, z);
					Vector3i posInWorld = ChunkUtils.localPositionToWorldPosition(chunk, posInChunk);
					if (posInWorld.y <= -5 && posInChunk.y != highEnd) {
						AbstractBlock block = blockSuppliers.get(RequiredAlias.DIRT).apply(posInChunk);
						if (block != null) {
							chunk.setLocalBlock(posInChunk, block);
						}
					}
				});
			});
		});

		return chunk;
	}

	public static enum RequiredAlias implements IRequiredAlias {
		DIRT;
	}

}
